; This file is part of Slime.
;
; Slime is free software: you can redistribute it and/or modify it
; under the terms of the GNU Affero General Public License as published
; by the Free Software Foundation, version 3.
;
; Slime is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU Affero General Public License for more details.
;
; You should have received a copy of the GNU Affero General Public License
; along with Slime. If not, see <https://www.gnu.org/licenses/>.

#include <build.Slime>
#include HEADER(common/mc146818)

#define IO_WAIT() \
        mov r0 0 $ mov r1 0x80 $ int ciel.syscall.out8

mc146818:
    .nmi: .pad 1 0 $ ..:

    .read: ; (integer r0) -> (integer r0)
        .save

        ; Select register
        and r0 0x7F
        lb r1 ..nmi
        sll r1 7
        or r0 r1
        mov r1 ..IO_CMOS0
        int ciel.syscall.out8
        IO_WAIT()

        ; Read from register
        mov r0 ..IO_CMOS1
        int ciel.syscall.in8
        sb .result r0

        .load
        lb r0 .result
        .return
        .result: .pad 1 0 $ ..:
    ..:

    .write: ; (integer r0, integer r1) -> ()
        .save
        mov r10 r0
        mov r0 r1

        ; Select register
        and r0 0x7F
        lb r1 ..nmi
        sll r1 7
        or r0 r1
        mov r1 ..IO_CMOS0
        int ciel.syscall.out8
        IO_WAIT()

        ; Write to register
        mov r0 r10
        mov r1 ..IO_CMOS1
        int ciel.syscall.out8

        .load
        .return
    ..:
..:
