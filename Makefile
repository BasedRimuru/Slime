# This file is part of Slime.
#
# Slime is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# Slime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Slime. If not, see <https://www.gnu.org/licenses/>.

.PHONY: all clean
all: init.bin
clean:
	@cat modules | grep -v '#' | while read -r module; do \
	    make -C "src/$${module}" clean ; \
	done
	rm -f init.Slime init.bin
	rm -f src/modules.Slime

init.bin: init.Slime
	tempest-as $< $@
init.Slime: src/base.Slime src/modules.Slime src/main.Slime
	cat $^ | cpp -Isrc -E -P > $@

src/modules.Slime: modules
	@printf "; Modules\n" > $@
	@cat modules | grep -v '#' | while read -r module; do \
	    ROOT="$(shell pwd)" make -C "src/$${module}" ; \
	    printf "#include HEADER(%s)\n" "$${module}" >> $@ ; \
	    printf "#include LINK(%s)\n" "$${module}" >> $@ ; \
	    printf "#define CONFIG_%s\n" \
	           "$$(printf '%s\n' $${module} | tr '/' '_' \
	                                        | tr '[:lower:]' '[:upper:]')" \
	           >> $@ ; \
	done
